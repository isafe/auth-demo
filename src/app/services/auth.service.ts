import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService {
  constructor(private http: Http) {
  }

  login(credentials) { 
   return this.http.post('/api/authenticate', 
      JSON.stringify(credentials)).
      map(response => {
        let token = response.json();
        if(token && token.token)
        {
          localStorage.setItem('token', token.token);
          return true;
        } else {
          return false;
        }
      });
  }

  logout() { 
    localStorage.removeItem('token');
  }
  isLoggedIn() { 
    //return tokenNotExpired();
    let jwtHelper = new JwtHelper();
    let token = localStorage.getItem('token');
    if(!token)
      return false;

    let expirationDate = jwtHelper.getTokenExpirationDate(token);
    let isExpired = jwtHelper.isTokenExpired(token);
    console.log('is expired', isExpired);
    return !isExpired;
  }

  get currentUser() {
    let token = localStorage.getItem('token');
    if(!token)
    return null;

    return new JwtHelper().decodeToken(token);
  }
}

