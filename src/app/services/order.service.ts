import { AuthHttp } from 'angular2-jwt';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class OrderService {
  constructor(private attp: AuthHttp){}

  getOrders() {
    return this.attp.get('/api/orders')
    .map(response => response.json());
  }

  /*
  constructor(private http: Http) {
  }

  getOrders() { 
    let header = new Headers();
    let token = localStorage.getItem('token');
    header.append('Authorization', 'Bearer ' + token);

    let options = new RequestOptions({ headers: header });

    return this.http.get('/api/orders', options)
      .map(response => response.json());
  } */

}
